const firebase = require('firebase');
require('firebase/firestore');

const firebaseConfig = require('./config.js');
const serviceAccount = require('./serviceAccount.json');
firebase.firestore(serviceAccount, firebaseConfig.databaseURL);


const db = firebase.firebase()

const read = async (book) => {
  try {
    const snapshot = db.collection(book).get()
    snapshot.forEach((doc) => {
      console.log(`${doc.id} => ${doc.data()}`);
    });
  } catch (err) {
    console.log(err);
  }
};

read('genesis');
