# README

1. Create a firebase bucket @ https://console.cloud.google.com/storage/browser/?project=jream-web, I called mine `holy-bible-esv`.
2. With the account going to be used, if other than project owner make sure import / export are enabled.
3. Set `glcoud` project with: `gcloud set project bible-app-esv`
4. To **Export** run: `gcloud firestore export gs://holy-bible-esv`
5. To **Import** run: `gcloud firestore import gs://holy-bible-esv` _(gs stands for Google Storage)_
6. See all Operations run: `gcloud firestore operations list` and `gcloud firestore operations describe [OPERATION_NAME]`
7. Cancel or Delete Operation run: `gcloud firestore operations cancel [OPERATION_NAME]` or `gcloud firestore operations delete [OPERATION_NAME]`

- Get `serviceAccount.json` key generated at the footer on: [https://console.firebase.google.com/u/0/project/bible-app-esv/settings](https://console.firebase.google.com/u/0/project/bible-app-esv/settings)

